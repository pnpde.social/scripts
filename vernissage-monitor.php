#!/usr/bin/php
<?
    require_once("/var/www/lib/d3/class.DBPostGres.php");
    
    $db = new DBPostGres('vernissage-db');
    
    $monitors = [
        // reports about statuses
        'reports_status' => [
            'hook'              =>  "vernissage_reports",
            'sql_new'           =>  "SELECT monitor.id, reporter.name as reporter, status.id as status_id, reported.name as reported, monitor.category as type
                                    	FROM public.\"Reports\" monitor
                                    	join public.\"Users\" reporter on monitor.\"userId\" = reporter.id
                                    	join public.\"Users\" reported on monitor.\"reportedUserId\" = reported.id
                                    	join public.\"Statuses\" status on monitor.\"statusId\" = status.id
                                    	where monitor.\"createdAt\" > now() - INTERVAL '3 days'
                                    ORDER BY monitor.id ASC ",
            'sql_maxid'         =>  "select max(id) from public.\"Reports\"",
            'report_item'       =>  function($row) {
                                        return $row->reporter.' meldete <https://vernissage.pnpde.social/statuses/'.$row->status_id.'> von '.$row->reported.' als **'.$row->type.'**';
                                    },
            'report_message'    =>  function($report_items) {
                                        return "neue Meldungen:\\n:small_orange_diamond: ".join("\\n:small_orange_diamond: ", $report_items)."\\nzur Moderation: <https://vernissage.pnpde.social/reports>";
                                    },
        ],

        // // new local profiles
        'profiles_new' => [
            'hook'              =>  "vernissage",
            'sql_new'           =>  "SELECT monitor.id, monitor.\"userName\" as username, monitor.name as name, monitor.\"createdAt\" as created_at
                                         FROM public.\"Users\" monitor
                                         where monitor.\"createdAt\" > now() - INTERVAL '3 days'
							    	        and monitor.\"isLocal\" = true
							    	        and monitor.\"deletedAt\" is null
                                            and monitor.id>0 ORDER BY monitor.id ASC",
            'sql_maxid'         =>  "select max(id) from public.\"Users\"",
            'report_item'       =>  function($row) {
                                        return $row->name." <https://vernissage.pnpde.social/@".$row->username."> created at ".$row->created_at;
                                    },
            'report_message'    =>  function($report_items) {
                                        return "neue Vernissage User:\\n:small_blue_diamond: ".join("\\n:small_blue_diamond: ", $report_items);
                                    },
        ],
        // template for monitors
        '_template' => [
            'sql_new'           =>  "",
            'sql_maxid'         =>  "",
            'report_item'       =>  function($row) {
                                        return "".$row->id;
                                    },
            'report_message'    =>  function($report_items) {
                                        return "".join("\\n:small_blue_diamond: ", $report_items);
                                    },
        ]
    ];
    
    
    foreach($monitors as $m => $monitor) {
        // skip templates
        if(preg_match('/^_/', $m)) {
            continue;
        }
        
        // default to 0
        $last_id = 0;
        
        // lockfiles contain the last ids we've seen
        $lockfile = '/var/lock/vernissage-monitor-'.$m.'.last';
        if(file_exists($lockfile)) {
            $last_id = file_get_contents($lockfile);
        }
        if(!$last_id || !is_numeric($last_id)) {
            $last_id=0;
        }

        // select items this monitor is interested in
        $report_items = [];
        $res = $db->query(preg_replace('/\b(order by)\b/i', "and monitor.id>$last_id $1", $monitor['sql_new']));
        while($row = pg_fetch_object($res)) {
            $report_items[] = $monitor['report_item']($row);
        }

        // send a message to discord if we have at least one item to report
        // uses hooker
        if(count($report_items)) {
            $message = $monitor['report_message']($report_items);
            system("hooker ".$monitor['hook']." \"$message\"");
            print $message;
        }

        // select max id and memorize it in our lockfile
        $res = $db->query($monitor['sql_maxid']);
        list($max_id) = pg_fetch_array($res);
        file_put_contents($lockfile, $max_id);
    }
?>
