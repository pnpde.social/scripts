#!/usr/bin/php
<?
    require_once("/var/www/dbconfig.php");
    require_once("/var/www/lib/d3/class.DBConn.php");
    
    $dbconn = new DBConn();
    $db = $dbconn->connect('pixelfed');
    
    $monitors = [
        // reports about statuses
        'reports_status' => [
            'hook'              =>  "pixelfed_reports",
            'sql_new'           =>  "select monitor.id, reporter.username as reporter, s.id as status_id, reported.username as reported, monitor.type as type
                                        from reports monitor
                                            join profiles reporter on monitor.profile_id = reporter.id
                                            join profiles reported on monitor.reported_profile_id = reported.id
                                            join statuses s on monitor.object_id = s.id
                                            where monitor.object_type = 'App\\\\Status'
                                            and monitor.created_at > now() - INTERVAL 3 day",
            'sql_maxid'         =>  "select max(id) from reports",
            'report_item'       =>  function($row) {
                                        return $row->reporter.' meldete <https://pixelfed.pnpde.social/i/web/post/'.$row->status_id.'> von '.$row->reported.' als **'.$row->type.'**';
                                    },
            'report_message'    =>  function($report_items) {
                                        return "neue Meldungen:\\n:small_orange_diamond: ".join("\\n:small_orange_diamond: ", $report_items)."\\nzur Moderation: <https://pixelfed.pnpde.social/i/admin/reports>";
                                    },
        ],
        // reports about non-statuses; haven't seen them yet
        'reports_profiles' => [
            'hook'              =>  "pixelfed_reports",
            'sql_new'           =>  "select monitor.id, reporter.username as reporter, s.id as status_id, reported.username as reported, monitor.type as type
                                        from reports monitor
                                            join profiles reporter on monitor.profile_id = reporter.id
                                            join profiles reported on monitor.reported_profile_id = reported.id
                                            join statuses s on monitor.object_id = s.id
                                            where monitor.object_type != 'App\\\\Status'
                                            and monitor.created_at > now() - INTERVAL 3 day",
            'sql_maxid'         =>  "select max(id) from remote_reports",
            'report_item'       =>  function($row) {
                                        return 'DEBUG: reports_profiles';
                                    },
            'report_message'    =>  function($report_items) {
                                        return 'DEBUG: reports_profiles';
                                    },
        ],
        // remote reports from e.g. Mastodon
        'reports_remote' => [
            'hook'              =>  "pixelfed_reports",
            'sql_new'           =>  "select monitor.id, monitor.comment, monitor.uri, monitor.report_meta, i.domain
                                        from remote_reports monitor
                                            join instances i on monitor.instance_id = i.id
                                            where monitor.created_at > now() - INTERVAL 3 day",
            'sql_maxid'         =>  "select max(id) from remote_reports",
            'report_item'       =>  function($row) {
                                        $meta = json_decode($row->report_meta);
                                        return 'Jemand auf '.$row->domain.' meldete <'.join('>, <', $meta->object).'>: '.$row->comment;
                                    },
            'report_message'    =>  function($report_items) {
                                        return "neue Meldungen von anderen Istanzen:\\n:small_orange_diamond: ".join("\\n:small_orange_diamond: ", $report_items)."\\nzur Moderation: <https://pixelfed.pnpde.social/i/admin/reports>";
                                    },
        ],
        // new local profiles
        'profiles_new' => [
            'hook'              =>  "pixelfed",
            'sql_new'           =>  "select monitor.id, monitor.username, monitor.name, monitor.created_at 
                                        from profiles monitor
                                            where monitor.domain is null 
                                            and monitor.deleted_at is null
                                            and monitor.created_at > now() - INTERVAL 3 day",
            'sql_maxid'         =>  "select max(id) from profiles",
            'report_item'       =>  function($row) {
                                        return $row->name." <https://pixelfed.pnpde.social/@".$row->username."> created at ".$row->created_at;
                                    },
            'report_message'    =>  function($report_items) {
                                        return "neue Pixelfed User:\\n:small_blue_diamond: ".join("\\n:small_blue_diamond: ", $report_items);
                                    },
        ],
        // template for monitors
        '_template' => [
            'sql_new'           =>  "",
            'sql_maxid'         =>  "",
            'report_item'       =>  function($row) {
                                        return "".$row->id;
                                    },
            'report_message'    =>  function($report_items) {
                                        return "".join("\\n:small_blue_diamond: ", $report_items);
                                    },
        ]
    ];
    
    
    foreach($monitors as $m => $monitor) {
        // skip templates
        if(preg_match('/^_/', $m)) {
            continue;
        }
        
        // default to 0
        $last_id = 0;
        
        // lockfiles contain the last ids we've seen
        $lockfile = '/var/lock/pixelfed-monitor-'.$m.'.last';
        if(file_exists($lockfile)) {
            $last_id = file_get_contents($lockfile);
        }
        if(!$last_id || !is_numeric($last_id)) {
            $last_id=0;
        }

        // select items this monitor is interested in
        $report_items = [];
        $res = $db->query($monitor['sql_new']." and monitor.id>$last_id");
        while($row = $res->fetch_object()) {
            $report_items[] = $monitor['report_item']($row);
        }

        // send a message to discord if we have at least one item to report
        // uses hooker
        if(count($report_items)) {
            $message = $monitor['report_message']($report_items);
            system("hooker ".$monitor['hook']." \"$message\"");
        }

        // select max id and memorize it in our lockfile
        $res = $db->query($monitor['sql_maxid']);
        list($max_id) = $res->fetch_array();
        file_put_contents($lockfile, $max_id);
    }
?>
