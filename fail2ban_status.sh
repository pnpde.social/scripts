#!/bin/bash

JAILS=`/usr/bin/fail2ban-client status |grep 'Jail list:'|sed -e 's/^.*list:\s*//; s/,//g'`
for J in $JAILS
do
    /usr/bin/fail2ban-client status $J
    echo ""
done 
