#!/bin/bash
TOOTCTL=/home/mastodon/live/bin/tootctl
START=`date +%s`

echo "[`date "+%Y-%m-%d %H:%M:%S"`] starting $1 maintenance"

export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"
export RAILS_ENV=production

if [ "$1" == "daily" ]
then
    # tootctl statuses remove
    #
    # Remove unreferenced statuses from the database, such as statuses that came 
    # from relays or from users who are no longer followed by any local accounts, 
    # and have not been replied to or otherwise interacted with.
    #
    # This is a computationally heavy procedure that creates extra database 
    # indices before commencing, and removes them afterward.
    #
    # Defaults to 90 days.
    #
    # https://docs.joinmastodon.org/admin/tootctl/#statuses-remove
    
    $TOOTCTL statuses remove
    
    # tootctl media remove
    #
    # Removes locally cached copies of media attachments, avatars or profile 
    # headers from other servers. By default, only media attachments are removed.
    #
    # Defaults to 7 days.
    #
    # https://docs.joinmastodon.org/admin/tootctl/#media-remove
    
    $TOOTCTL media remove
    
    # tootctl preview_cards remove
    #
    # Remove local thumbnails for preview cards.
    #
    # Defaults to 180 days.
    #
    # NOTE: it is not recommended to delete preview cards within the last 14 
    # days, because preview cards will not be refetched unless the link is 
    # reposted after 2 weeks from last time.)
    #
    # https://docs.joinmastodon.org/admin/tootctl/#preview_cards
    
    $TOOTCTL preview_cards remove --days 60
fi

if [ "$1" == "weekly" ]
then
    # tootctl accounts cull
    #
    # Remove remote accounts that no longer exist. Queries every single remote 
    # account in the database to determine if it still exists on the origin 
    # server, and if it doesn't, then remove it from the database. Accounts that 
    # have had confirmed activity within the last week are excluded from the 
    # checks, in case the server is just down.
    #
    # https://docs.joinmastodon.org/admin/tootctl/#accounts-cull
    
    $TOOTCTL accounts cull
    
    # tootctl accounts prune
    #
    # Like tootctl statuses remove, This command will delete any remote account 
    # that is never interacted with local. It removes many avatar/header images.
    # 
    # https://github.com/mastodon/mastodon/pull/18397
    
    $TOOTCTL accounts prune
fi

if [ "$1" == "monthly" ]
then
    # tootctl media remove-orphans
    #
    # Scans for files that do not belong to existing media attachments, and 
    # remove them. Please mind that some storage providers charge for the 
    # necessary API requests to list objects. Also, this operation requires 
    # iterating over every single file individually, so it will be slow.
    #
    # https://docs.joinmastodon.org/admin/tootctl/#media-remove-orphans
    
    $TOOTCTL media remove-orphans
fi;

END=`date +%s`
DURATION=$(( $END - $START ))

echo "[`date "+%Y-%m-%d %H:%M:%S"`] finishing $1 maintenance. took $DURATION seconds"
