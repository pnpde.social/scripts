#!/bin/bash

set -e

DATE=`date +%w_%a_%H`

function dump {
    # dumps to psqls custom format which is indicated by .c filename extension
    DATABASE=$1
    PORT=$2
    USER=$3
    
    DUMPFILE=/var/backups/postgres/$DATABASE.$DATE
    
    echo "$DATABASE"
    echo "DUMP     $DATABASE to $DUMPFILE.tmp"
    #if [[ $PORT ]] && [[ $USER ]]
    #then
    #    # TODO --cluster 15/main is atm only needed for bookwyrm
    #    su - postgres -c "pg_dump -Fc -h localhost -p $PORT -U $USER $DATABASE --cluster 15/main > $DUMPFILE.tmp 2> $DUMPFILE.log"
    #else
        su - postgres -c "pg_dump -Fc $DATABASE > $DUMPFILE.tmp 2> $DUMPFILE.log"
    #fi
    
    # integrity check
    echo "CHECK    $DUMPFILE.tmp"
    pg_restore -l $DUMPFILE.tmp >/dev/null
    
    # move
    echo "MOVE     $DUMPFILE.tmp => $DUMPFILE.c"
    mv $DUMPFILE.tmp $DUMPFILE.c
    
    # cleanup logs
    if [[ ! -s $DUMPFILE.log ]]
    then
        rm $DUMPFILE.log
    else
        echo " !!!     left some messages in $DUMPFILE.log"
    fi
    echo ""
}

if [ -z "$1" ] 
then
    date
    echo ""
    
    # globals
    echo "globals"
    DUMPFILE=/var/backups/postgres/globals.$DATE
    echo "DUMP     globals to $DUMPFILE.sql"
    su - postgres -c "pg_dumpall -g -f $DUMPFILE.sql > $DUMPFILE.log 2>&1"
    echo "COMPRESS $DUMPFILE.sql to .gz"
    gzip -f $DUMPFILE.sql
    if [[ ! -s $DUMPFILE.log ]]
    then
        rm $DUMPFILE.log
    else
        echo " !!!     left some messages in $DUMPFILE.log"
    fi
    echo ""
    
    dump postgres
    dump mastodon_production 
    dump vernissage
    dump gotosocial
    # dump bookwyrm 6432 pgdump   # bookwyrm is dockerized
    
    echo "-- "
else
    dump $1
fi
    
exit 0