<?
    require_once("/var/www/dbconfig.php");
    require_once("/var/www/lib/d3/class.DBConn.php");
    
    $dbconn = new DBConn();
    $dblog = $dbconn->connect('logstack');
    $dbwiki = $dbconn->connect('bookstack');
    
    $items = [];

    // select create and update events from yesterday to 1 week old
    // newest first
    $res = $dblog->query("select id, event, text, url, triggered_at, triggered_by_name 
        from event 
        where event in('page_create', 'page_update')
        and id not in (select event_id from toot where status in ('published', 'ignored'))
        and createtime > NOW() - INTERVAL 1 WEEK
        and createtime < NOW() - INTERVAL 1 DAY 
        order by createtime asc");
    
    while(list($id, $event, $text, $url, $triggered_at, $triggered_by_name) = $res->fetch_row()) {
        print "$url\n";
        
        $slug = preg_replace('#^.*/books/.*?/page/#', '', $url);

        // select page and book id to slug
        $resPage = $dbwiki->query("select id, book_id, name, html 
                from pages 
                    where slug like '$slug'
                    and deleted_at is null
                    and draft = 0");
        while(list($pageid, $bookid, $pagename, $html) = $resPage->fetch_row()) {
            
            // select permissions. public viewable pages return num_rows=0 or view=0
            // role 0 = public, role 4 = guest
            $resPerm = $dbwiki->query("select view
                        from entity_permissions
                        where (
                            (entity_type='book' and entity_id = $bookid)
                            or 
                            (entity_Type='page' and entity_id = $pageid)
                        )
                        and role_id in (0, 4)
                        and view = 0");

            

            // at least one row found
            if(!$resPerm->num_rows) {
                //// with !$resPerm->num_rows the check on $view makes no sense, does it? removing it for now
                ////
                //// // find lowest score on view permission
                //// while(list($view) = $resPerm->fetch_row()) {
                ////     $view = min($view);
                //// }
                //// 
                //// print $view."\n";
                //// 
                //// // is view > 0
                //// if($view) {
                    // if its an update replace url with url to latest revision
                    if($event == 'page_update' && $pageid) {
                        $resRev = $dbwiki->query("select max(id) from page_revisions where page_id=$pageid");
                        list($revision) = $resRev->fetch_row();
                        $url = $url ."/revisions/$revision/changes";
                    }
                    
                    $items[$slug] = [
                        'id' => $id,
                        'title' => $pagename,
                        'description' => strip_tags(html_entity_decode($html)),
                        'link' => $url,
                        'creator' => $triggered_by_name,
                        'guid' => 'logstack'.$id.'@wiki.pnpde.social',
                        'pubdate' => $triggered_at,
                        'event' => $event,
                    ];
                //// }
            }
            // every other event is ignored
        }
        
        // usually 10 is plenty
        // todo: check if an item has a link and break after that instead
        if(count(array_keys($items))>10) {
            break;
        }
    }
    
    foreach($items as $slug => $item) {
        if($item['link']) {
            
            // build a nice toot
            if($item['event'] == 'page_update') {
                $toot = 'Update: ';
            } else {
                $toot = 'Neu: ';
            }
            $words = explode(' ', $item['title']." ".$item['link']."\n\n".$item['description']);
            foreach($words as $word) {
                if(strlen($toot." ".$word)>490) {
                    $toot .= " [...]";
                    break;
                }
                $toot .= $word." ";
            }
            
            // toot it
            // print trim($toot)."\n\n---\n";
            $toot_url = preg_replace('/Toot\s+posted:\s*/i', '', trim(`/usr/bin/toot post -u wiki@bot.pnpde.social "$toot"`));
            print $toot_url."\n";
            
            // memorize it
            $dblog->query("replace into toot (event_id, status, toot_url) values (".$item['id'].", 'published', '".$toot_url."')");
            
            // ignore remaining events to the same page
            $dblog->query("insert into toot (event_id, status) 
                    select id, 'ignored' from event 
                        where url = (select url from event where id=".$item['id'].")
                        and id not in (select event_id from toot)
                        and createtime > DATE(NOW() - INTERVAL 1 WEEK )
                        and createtime < DATE(NOW() - INTERVAL 1 HOUR )");
            // so on the next run, there won't be events for this page to process
            exit;
        }
    }
?>
