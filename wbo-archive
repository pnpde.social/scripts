#!/bin/bash

########################################################################
# archive wbo boards
#
# this script archives wbo json files when they haven't been modified
# for a long time (TARCHIVE) using gzip. those archives are kept for
# a shorter time (TREMOVE) and then removed
#
# 2024 Thomas Nesges for pnpde.social
########################################################################

# config
# directory where wbos json files are kept
WBODATADIR="/opt/whitebophir/server-data"
# json files older then TARCHIVE are archived
TARCHIVE=366
# gz files order then TREMOVE are deleted
TREMOVE=92

########################################################################

# current date for the logs
date +"%Y-%M-%D %H:%I:%S"

# count boards that are older than TARCHIVE and need to be archived
COUNT=`find $WBODATADIR -name 'board*.json' -mtime +$TARCHIVE | wc -l`
if [[ $COUNT > 0 ]]
then
    echo "archiving $COUNT boards older then $TARCHIVE days"
    # gzip the json file, then touch json.gz to update its mtime
    # otherwise it would keep it's original mtime 
    find $WBODATADIR -name 'board*.json' -mtime +$TARCHIVE -exec gzip {} \; -exec touch {}.gz \;
else
    echo "nothing to archive"
fi

# count all gz files
GZCOUNT=`find $WBODATADIR -name 'board*.json.gz' | wc -l`
if [[ $GZCOUNT > 0 ]]
then
    echo "have $GZCOUNT archived boards"

    # count gz files that are older then TREMOVE
    RMCOUNT=`find $WBODATADIR -name 'board*.json.gz' -mtime +$TREMOVE | wc -l`
    if [[ $RMCOUNT > 0 ]]
    then
        # delete the gz file
        echo "deleting $RMCOUNT archived boards older then $TREMOVE days"
        find $WBODATADIR -name 'board*.json.gz' -mtime +$TREMOVE -exec rm {} \;
    else
        echo "have no boards to be removed"
    fi
else
    echo "have no archived boards"
fi