<?
    // localfeed.php
    // 2023 by Thomas Nesges for pnpde.social
    //
    // Installation:
    // 1. copy localfeed.php to your php enabled webserver
    // 2. mkdir cache (optional)
    // 3. set $cachetime variable (optional)
    // 4. set $default_server to a mastodon domain (optional)
    
    $cachetime      = 60*5; // seconds; 0 to disable caching
    $default_server = 'mastodon.pnpde.social'; // mastodon domain for which rss is generated when no server is specified via get parameter ?server
    
    // Usage:
    // point your feedreader to
    // https://web.serv.er/localfeed.php?server=mastodon.domain
    // or https://web.serv.er/localfeed.php
    
    header('Content-type: application/rss+xml; charset=utf-8');
    ob_start();

    $server = isset($_GET['server']) ? htmlspecialchars($_GET['server']) : $default_server;

    if(! is_dir('cache')) {
        @mkdir('cache'); // try to mkdir, but this may not work because of missing permissions
    }
    if(! is_dir('cache')) {
        // if we still don't have a cache directory, just store cache-files in .
        // though it is likely that we can't write to . because of permissions
        $cachefile = "cache_".md5($server).".rss";
    } else {
        $cachefile = "cache/".md5($server).".rss";
    }

    // mastodon api calls
    $instance = json_decode(file_get_contents('https://'.$server.'/api/v1/instance'));
    $timeline = json_decode(file_get_contents('https://'.$server.'/api/v1/timelines/public?local=true&only_media=false'));
    
    // read cachefile, if it exists and is newer than $cachetime, or if mastodon api didn't respond
    if($cachetime && file_exists($cachefile) && (filemtime($cachefile) > time() - $cachetime || !$instance || !$timeline)) {
        readfile($cachefile);
        exit;
    }
    // exit, if mastodon api didn't respond 
    if(!$instance || !$timeline) {
        exit;
    }
    
    $feedlanguage = $instance->languages[0];

    print "<?xml version='1.0' encoding='UTF-8'?>\n";
?>
<rss version="2.0" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:atom="http://www.w3.org/2005/Atom">
    <channel> 

        <title><?= rssencode($instance->title) ?></title>
        <link>https://<?= $server ?></link>
        <description><?= rssencode($instance->short_description) ?></description>
        <language><?= $feedlanguage ?></language>
        <pubDate><?= date(DATE_RFC822) ?></pubDate>
        <lastBuildDate><?= date(DATE_RFC822) ?></lastBuildDate>
        <image>
            <url><?= $instance->thumbnail ?></url>
            <title><?= rssencode($instance->title) ?></title>
            <link>https://<?= $server ?></link>
        </image>
        <generator>localfeed.php by Thomas Nesges, https://codeberg.org/pnpde.social/scripts/src/branch/main/localfeed.php</generator>
        <ttl><?= $cachetime ? $cachetime/60 : 60 ?></ttl>
        <atom:link href="<?= (empty($_SERVER['HTTPS']) ? 'http' : 'https').'://'.$_SERVER['SERVER_NAME'].($_SERVER['SERVER_PORT']!=443 ? ':'.$_SERVER['SERVER_PORT'] : '').$_SERVER['PHP_SELF'] ?>" rel="self" type="application/rss+xml" />
<?
    foreach($timeline as $toot) {
        if($toot->spoiler_text) {
            $text = "";
            $title = '[CN] '.$toot->spoiler_text;
            
            $description = $feedlanguage=='de' ? 'Dieser Eintrag hat eine Inhaltswarnung und wird deshalb im Feed nicht angezeigt.' : 'This entry has a content notice and is therefore not displayed in the feed.';
        } else {
            $text = trim(strip_tags($toot->content));
            // first search for a space char
            $pos = strpos($text, ' ', min(strlen($text), 80));
            if(!$pos) {
                // then search for a .
                $pos = strpos($text, '.', min(strlen($text), 80));
            }
            if(!$pos) {
                // then copy whole $text
                $pos = strlen($text);
            }
            $title = substr($text, 0, $pos);
            
            if(!$title) {
                $title = $text;
            }
            if(!$title) {
                // if $text is empty
                $title = $feedlanguage=='de' ? "ohne Titel" : "untitled";
            }
            
            $description = rssencode($toot->content);
        }
        
        ?>
        
        <item>
            <title><?= rssencode($title) ?></title>
            <link><?= $toot->url ?></link>
            <description><![CDATA[<?= $description ?>]]></description>
            <dc:creator><?= '@'.rssencode($toot->account->username).'@'.$instance->uri.' ('.$toot->account->display_name.')' ?></dc:creator>
            <guid isPermaLink='true'><?= $toot->uri ?></guid>
            <pubDate><?= date(DATE_RFC822, strtotime($toot->created_at)) ?></pubDate>
<?
            foreach($toot->tags as $tag) {
                print "            <category>".$tag->name."</category>\n";
            }
?>
        </item>
        <?
    }
?>

    </channel>
</rss>
<?
    $rss = ob_get_flush();
    
    // save to cache
    if($cachetime && $cachefile) {
        file_put_contents($cachefile, $rss);
    }
    
    function rssencode($string) {
        return preg_replace('/&/', '&amp;', html_entity_decode($string));
    }
?>