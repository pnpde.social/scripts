<?
    header("Content-type: text/plain");
    
    // -----------------------------------------------------------------
    // NOTIFY
    // -----------------------------------------------------------------
    
    // OGHMA
    $df = preg_split("#[\r\n]+#", trim(shell_exec("df -h")));
    $out = [];
    for($d=0; $d<count($df); $d++) {
        if(preg_match('#^/dev/#', $df[$d])) {
            $out[$d] = $df[$d];
            if(preg_match('/(99%)/', $out[$d])) {
                notify('ERROR', $df[$d]); // >= 99%
            } else if(preg_match('/(9[6-8]%)/', $out[$d])) {
                notify('URGENT', $df[$d]); // >= 96%
            } else if(preg_match('/(9[3-5]%)/', $out[$d])) {
                notify('WARN', $df[$d]); // >= 93%
            } else if(preg_match('/(9.%)/', $out[$d])) {
                notify('INFO', $df[$d]); // > 90%
            } else if(preg_match('/([5678]\d%)/', $out[$d])) {
                //notify('INFO', $df[$d]); // > 50%
            }
        }
    }
    
    $host = trim(`/usr/bin/hostname`);
    // only on oghma
    if($host=='oghma') {
        // KELEMVOR
        $df = preg_split("#[\r\n]+#", trim(shell_exec("ssh kelemvor df -h")));
        $out = [];
        for($d=0; $d<count($df); $d++) {
            if(preg_match('#^u338142#', $df[$d])) {
                $out[$d] = $df[$d];
                if(preg_match('/(100%)/', $out[$d])) {
                    notify('ERROR', $df[$d]); // full
                } else if(preg_match('/(9[5-9]%)/', $out[$d])) {
                    notify('URGENT', $df[$d]); // >= 95%
                } else if(preg_match('/(9.%)/', $out[$d])) {
                    notify('WARN', $df[$d]); // >= 90%
                } else if(preg_match('/(8.%)/', $out[$d])) {
                    notify('INFO', $df[$d]); // >= 80%
                } else if(preg_match('/([567]\d%)/', $out[$d])) {
                    // ignore
                }
            }
        }
    }
    
    // $out = join("\n", $out);
    // print $out; 
    
    // -----------------------------------------------------------------
    // MONITOR
    // -----------------------------------------------------------------

    require_once("/var/www/dbconfig.php");
    require_once("/var/www/lib/d3/class.DBConn.php");
    $dbconn = new DBConn();
    $db = $dbconn->connect('pnpde_social');

    // OGHMA
    $host = trim(`/usr/bin/hostname`);
    foreach(preg_split("#[\r\n]+#", trim(shell_exec("df"))) as $line) {
        if(preg_match('#^/dev/#', $line)) {
            logline($line);
        }
    }
    
    // only on oghma
    if($host=='oghma') {
        // KELEMVOR
        $host="kelemvor";
        foreach(preg_split("#[\r\n]+#", trim(shell_exec("ssh kelemvor df -k"))) as $line) {
            if(preg_match('#^u338142#', $line)) {
                logline($line);
            }
        }
    }
   
    // -----------------------------------------------------------------

    function logline(string $line) : void {
        global $host, $db;
        
        $f = preg_split('/\s+/', $line);
        $dev = $f[0];
        $size = $f[1];
        $used = $f[2];
        $avail = $f[3];
        $percused = $f[4];
        $mount = $f[5];

        $sql = "insert into df (server, device, size, used, avail, percused, mountpoint) values (?,?,?,?,?,?,?)";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("ssiiiis", $host, $dev, $size, $used, $avail, $percused, $mount);
        $stmt->execute();
        $stmt->close();
    }

    function notify(string $level, string $df) : void {
        $f = preg_split('/\s+/', $df);
        $dev = $f[0];
        $size = $f[1];
        $used = $f[2];
        $avail = $f[3];
        $percused = $f[4];
        $mount = $f[5];
        
        $maxage = 60*60*48;
        if($level == 'DEBUG' && isset($_GET['DEBUG']) && $_GET['DEBUG']=='1') {
            $maxage = 0;
        } else if($level == 'INFO') {
            $maxage = 60*60*24*31; // 31d
        } else if($level == 'WARN') {
            $maxage = 60*60*24*7; // 7d
        } else if($level == 'URGENT') {
            $maxage = 60*60*24;  // 1d 
        } else if($level == 'ERROR') {
            $maxage = 60*30;    // 30m
        }
        
        $notified = '/var/log/df/df-notify.'.md5($dev).'.'.$level;
        if(!file_exists($notified) || filemtime($notified) < time()-$maxage) {
            $host = strtoupper(trim(`/usr/bin/hostname`));

            $headers = [];
            $headers[] = "From: ".$host." <noreply@pnpde.social>";
            $headers[] = "MIME-Version: 1.0";
            $headers[] = "Content-type:text/html;charset=UTF-8";

            mail('admin@pnpde.social', "[pnpde.social] $level: Volume $dev on $host is $percused full", "<pre>".trim(shell_exec('df -h'))."</pre>", join("\r\n", $headers));
            system("hooker system $level: Volume $dev on $host is $percused full");
            print "notification sent";
            
            touch($notified);
        }
    }
?>
