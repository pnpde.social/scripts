#!/bin/bash

# DON'T use this script without understanding EVERY SINGLE STEP
# it's barely working on our system and may just BREAK YOURS!
# plus: it's very specific to our system and you'll HAVE to adjust it

if [ `whoami` != "root" ]
then 
    echo "root only"
    exit
fi

echo "path to mastodon? [/home/mastodon/live] "
read MASTODON
if [ "$MASTODON" == "" ] 
then
    MASTODON=/home/mastodon/live
fi;

echo "here are the latest releases for $MASTODON:"
su mastodon -c "cd $MASTODON; git ls-remote --tags | tail -n 5"
echo ""
echo "update to release? "
read TOVERSION
if [ "$TOVERSION" == "" ] 
then
    echo "don't know what to make of that"
    exit
fi

echo ""
echo "--------------------------------------------------------------------"
echo "DON'T use this script."
echo "Really: Don't! It's highly experimental and a work in progress"
echo ""
echo "DON'T use this script without understanding EVERY SINGLE STEP"
echo "it's barely working on our system and may just BREAK YOURS!"
echo "plus: it's very specific to our system and you'll HAVE to adjust it"
echo "--------------------------------------------------------------------"
echo ""
echo "continue? [n]"
read CONTINUE
if [ "$CONTINUE" != "y" ] 
then
    echo "bye"
    exit
fi

#----------------------------------------------------------------------
# BACKUP
#----------------------------------------------------------------------

echo "make a fresh backup? [y]"
read DOBACKUP
if [ "$DOBACKUP" == "y" ] || [ "$DOBACKUP" == "" ] 
then
    # root
    # make a fresh backup
    echo ""
    echo "dumping postgres..."
    /root/maintenance/postgresdump.sh
    # grab a coffee, borg may take some time
    echo ""
    echo "running borg backup (this may take some time)..."
    /usr/local/bin/borg-backup.sh
 
    # delete old working backup and make a new one
    echo "making fresh working backup of $MASTODON in $MASTODON.copy"
    rm -rf $MASTODON.copy
    rsync -a $MASTODON $MASTODON.copy --exclude live/public/system/

    echo "backups done. continue? [y]"
    read CONTINUE
    if [ "$CONTINUE" != "n" ] && [ "$CONTINUE" != "N" ]
        then
            exit
    fi
fi

#----------------------------------------------------------------------
# UPDATE
#----------------------------------------------------------------------

cd $MASTODON

echo ""
echo "start regular update process? [y]"
read DOUPDATE
if [ "$DOUPDATE" == "y" ] || [ "$DOUPDATE" == "" ]
then
    # start regular update process
    su mastodon -c "cd $MASTODON && git fetch && git checkout $TOVERSION"
    su mastodon -c "cd $MASTODON && bundle install"
    su mastodon -c "cd $MASTODON && yarn install"

    echo "first part done. continue? [y]"
    read CONTINUE
    if [ "$CONTINUE" != "n" ] && [ "$CONTINUE" != "N" ]
    then
        exit
    fi
fi

#----------------------------------------------------------------------
# CUSTOM CHANGES
#----------------------------------------------------------------------

echo ""
echo "apply custom changes? [y]"
read DOCUSTOM
if [ "$DOCUSTOM" == "y" ] || [ "$DOCUSTOM" == "" ]
then
    # check and repair custom theme configuration
    echo "checking themes:"
    for THEME in pnpde pnpde-low-contrast
    do
        THEMES=$(yq ".\"$THEME\"" $MASTODON/config/themes.yml 2> /dev/null)
        if [ "$THEMES" == "\"styles/$THEME.scss\"" ]
            then echo " OK $MASTODON/config/themes.yml knows about $THEMES"
            else 
                echo " REPAIR $MASTODON/config/themes.yml: adding $THEME"
                echo "$THEME: styles/$THEME.scss" >> $MASTODON/config/themes.yml
        fi
        
        for CC in de en
        do
            LOCALE=$(yq ".\"$CC\".themes.\"$THEME\"" $MASTODON/config/locales/$CC.yml 2> /dev/null)
            if [ "$LOCALE" != "null" ] && [ "$LOCALE" != "" ]
                then echo " OK    $MASTODON/config/locales/$CC.yml for $THEME is $LOCALE"
                else 
                    echo " REPAIR $MASTODON/config/locales/$CC.yml: adding $THEME"
                    if [ "$CC" == "de" ]
                        then
                            perl -i -ne 's/(\s+themes:.*?)/$1\n    pnpde: pnpde.social\n    pnpde-low-contrast: pnpde.social (Niedriger Kontrast)/s;  print' $MASTODON/config/locales/$CC.yml
                        else
                            perl -i -ne 's/(\s+themes:.*?)/$1\n    pnpde: pnpde.social\n    pnpde-low-contrast: pnpde.social (low contrast)/s;  print' $MASTODON/config/locales/$CC.yml
                    fi
            fi
        done
    done
    
    echo "themes done. continue? [y]"
    read CONTINUE
    if [ "$CONTINUE" != "n" ] && [ "$CONTINUE" != "N" ]
    then
        exit
    fi
    
    
    echo "checking application.html.haml:"
    for LINE in "%script{ src: 'https://pnpde.social/js/jquery.js'}" "%script{ src: 'https://pnpde.social/js/navbar.js'}" "%script{ src: 'https://blue.pnpde.social/js/mastodon.js'}" "%script{ src: 'https://mastodon.pnpde.social/custom/pnpde.social.js'}" "= stylesheet_link_tag 'https://blue.pnpde.social/css/mastodon.css', skip_pipeline: true, media: 'all'" "= stylesheet_link_tag 'https://mastodon.pnpde.social/custom/pnpde.social.css', skip_pipeline: true, media: 'all'"
    do
        grep --quiet -E "$LINE" $MASTODON/app/views/layouts/application.html.haml 2> /dev/null
        if [ $? == 0 ]
            then echo " OK    $LINE found"
            else 
                echo " REPAIR adding $LINE";
                perl -i -ne "s#((^\s+)= yield :header_tags)#\2$LINE\n\1#s; print" $MASTODON/app/views/layouts/application.html.haml
        fi
    done
    echo ""
    
    echo "application.html.haml done. continue? [y]"
    read CONTINUE
    if [ "$CONTINUE" != "n" ] && [ "$CONTINUE" != "N" ]
    then
        exit
    fi

    
    CSPERROR=0
    echo "checking content_security_policy.rb:"
    for LINE in "p.style_src       :self, \"pnpde.social\", \"blue.pnpde.social\", assets_host, :unsafe_inline" "p.connect_src :self, \"pnpde.social\", \"blue.pnpde.social\", :data, :blob, assets_host, media_host, Rails.configuration.x.streaming_api_base_url, \*webpacker_urls" "p.script_src  :self, \"pnpde.social\", \"blue.pnpde.social\", :unsafe_inline, :unsafe_eval, assets_host" "p.connect_src :self, \"pnpde.social\", \"blue.pnpde.social\", :data, :blob, assets_host, media_host, Rails.configuration.x.streaming_api_base_url" "p.script_src  :self, \"pnpde.social\", \"blue.pnpde.social\", assets_host, \"'wasm-unsafe-eval'\", :unsafe_inline" "p.script_src :self, \"pnpde.social\", \"blue.pnpde.social\", :unsafe_inline, assets_host" "p.style_src  :self, \"pnpde.social\", \"blue.pnpde.social\", :unsafe_inline, assets_host" 
    do
        grep --quiet -E "$LINE" $MASTODON/config/initializers/content_security_policy.rb 2> /dev/null
        if [ $? == 0 ]
            then echo " OK    $LINE found"
            else echo " ERROR $LINE not found"; CSPERROR=1
        fi
    done
    
    if [ $CSPERROR > 0 ] 
    then
        echo ""
        echo " REPAIR content_security_policy.rb"
        for LINE in '(p.connect_src\s+:self,)(\s+:data)' '(p.script_src\s+:self,)(\s+:unsafe_inline)' '(p.script_src\s+:self,)(\s+assets_host)' '(p.style_src\s+:self,)(\s+assets_host)' '(p.style_src\s+:self,)(\s+:unsafe_inline)' 
        do
            perl -i -ne "s/$LINE/\1 \"pnpde.social\", \"blue.pnpde.social\",\2/; print" $MASTODON/config/initializers/content_security_policy.rb
        done
    fi
    
    echo "csp done. continue? [y]"
    read CONTINUE
    if [ "$CONTINUE" != "n" ] && [ "$CONTINUE" != "N" ]
    then
        exit
    fi
fi

#----------------------------------------------------------------------
# CONTINUE UPDATE
#----------------------------------------------------------------------

echo ""
echo "precompile assets? [y]"
read DOPRECOMPILE
if [ "$DOPRECOMPILE" == "y" ] || [ "$DOPRECOMPILE" == "" ]
then
    # delete webpack cache and precompile
    rm public/packs/manifest.json*
    su mastodon -c "cd $MASTODON && RAILS_ENV=production bundle exec rails assets:precompile"

    echo "precompile done. continue? [y]"
    read CONTINUE
    if [ "$CONTINUE" != "n" ] && [ "$CONTINUE" != "N" ]
    then
        exit
    fi
fi

#----------------------------------------------------------------------
# RESTART
#----------------------------------------------------------------------

echo ""
echo "restart services? [y]"
read DORESTART
if [ "$DORESTART" == "y" ] || [ "$DORESTART" == "" ]
then
    # root
    systemctl restart mastodon-sidekiq mastodon-streaming mastodon-web
fi
echo ""
echo "all done."