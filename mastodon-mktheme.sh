THEMENAME=$1
if [ $THEMENAME ]
then
    if [ -e ~/live/app/javascript/styles/$THEMENAME ]
    then
        echo "Ein Theme $THEMENAME gibt es bereits. Breche ab."
        exit
    fi
    cd ~/live/app/javascript/styles
    mkdir "$THEMENAME"
    cp mastodon/variables.scss "$THEMENAME/"
    touch "$THEMENAME/diff.scss"
    cat << EOT >> "$THEMENAME.scss"
@import '$THEMENAME/variables';
@import 'application';
@import '$THEMENAME/diff';
EOT
    cd ~/live/config
    echo "$THEMENAME: styles/$THEMENAME.scss" >> themes.yml
    cd ~/live/app/javascript/styles

    echo Du findest dein Theme in "~/live/app/javascript/styles/$THEMENAME"
    echo Du kannst jetzt variables.scss anpassen
    echo Optional kannst du in ~/live/config/locales/de.yml unter \"themes\" eine Zeile \"$THEMENAME: Menschenlesbare Bezeichnung\" einfuegen
    echo Danach muessen Assets kompiliert werden:
    echo '  cd ~/live'
    echo '  RAILS_ENV=production bundle exec rails assets:precompile'
    echo Starte danach den service mastodon-web neu
else
    echo "Bitte mktheme.sh THEMENAME"
fi