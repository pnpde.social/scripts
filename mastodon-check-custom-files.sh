MASTODON="/home/mastodon/live"
ERRORS=0
WARNINGS=0

for THEME in pnpde pnpde-low-contrast
do
    echo "checking theme $THEME:"
    if [ -f $MASTODON/app/javascript/styles/$THEME.scss ]
        then echo " OK    file $MASTODON/app/javascript/styles/$THEME.scss exists"
        else echo " ERROR file $MASTODON/app/javascript/styles/$THEME.scss doesn't exist"; ERRORS=$((ERRORS+1));
    fi
    
    if [ -d $MASTODON/app/javascript/styles/$THEME/ ]
        then echo " OK    dir $MASTODON/app/javascript/styles/$THEME/ exists"
        else echo " ERROR dir $MASTODON/app/javascript/styles/$THEME/ doesn't exist"; ERRORS=$((ERRORS+1));
    fi
    
    THEMES=$(yq ".\"$THEME\"" $MASTODON/config/themes.yml 2> /dev/null)
    if [ "$THEMES" == "\"styles/$THEME.scss\"" ]
        then echo " OK    $MASTODON/config/themes.yml knows about $THEMES"
        else echo " ERROR $MASTODON/config/themes.yml doesn't know about $THEME"; ERRORS=$((ERRORS+1));
    fi
    
    for CC in de en
    do
        LOCALE=$(yq ".\"$CC\".themes.\"$THEME\"" $MASTODON/config/locales/$CC.yml 2> /dev/null)
        if [ "$LOCALE" != "null" ] && [ "$LOCALE" != "" ]
            then echo " OK    $MASTODON/config/locales/$CC.yml for $THEME is $LOCALE"
            else echo " WARN  $MASTODON/config/locales/$CC.yml doesn't know about $THEME"; WARNINGS=$((WARNINGS+1));
        fi
    done
    echo ""
done

echo "checking application.html.haml:"
for LINE in "%script{ src: 'https://pnpde.social/js/jquery.js\?v=[0-9]+'}" "%script{ src: 'https://pnpde.social/js/navbar.js\?v=[0-9]+'}" "%script{ src: 'https://blue.pnpde.social/js/mastodon.js\?v=[0-9]+'}" "%script{ src: 'https://mastodon.pnpde.social/custom/pnpde.social.js\?v=[0-9]+'}" "= stylesheet_link_tag 'https://blue.pnpde.social/css/mastodon.css\?v=[0-9]+', skip_pipeline: true, media: 'all'" "= stylesheet_link_tag 'https://mastodon.pnpde.social/custom/pnpde.social.css\?v=[0-9]+', skip_pipeline: true, media: 'all'"
do
    grep --quiet -E "$LINE" $MASTODON/app/views/layouts/application.html.haml 2> /dev/null
    if [ $? == 0 ]
        then echo " OK    $LINE found"
        else echo " ERROR $LINE not found"; ERRORS=$((ERRORS+1));
    fi
done
echo ""

echo "checking content_security_policy.rb:"
for LINE in "p.style_src       :self, \"pnpde.social\", \"blue.pnpde.social\", assets_host" "p.connect_src :self, \"pnpde.social\", \"blue.pnpde.social\", :data, :blob, assets_host, \*media_hosts, Rails.configuration.x.streaming_api_base_url, \*webpacker_urls, \*front_end_build_urls" "p.script_src  :self, \"pnpde.social\", \"blue.pnpde.social\", :unsafe_inline, :unsafe_eval, assets_host" "p.connect_src :self, \"pnpde.social\", \"blue.pnpde.social\", :data, :blob, assets_host, \*media_hosts, Rails.configuration.x.streaming_api_base_url" "p.script_src  :self, \"pnpde.social\", \"blue.pnpde.social\", assets_host, \"'wasm-unsafe-eval'\"" "p.script_src :self, \"pnpde.social\", \"blue.pnpde.social\", :unsafe_inline, assets_host" "p.style_src  :self, \"pnpde.social\", \"blue.pnpde.social\", :unsafe_inline, assets_host" 
do
    grep --quiet -E "$LINE" $MASTODON/config/initializers/content_security_policy.rb 2> /dev/null
    if [ $? == 0 ]
        then echo " OK    $LINE found"
        else echo " ERROR $LINE not found"; ERRORS=$((ERRORS+1));
    fi
done
if [ $WARNINGS != 0 ] || [ $ERRORS != 0 ]
then
    echo "------------"
    echo " $WARNINGS Warnings"
    echo " $ERRORS Errors"
else
    echo "----"
    echo " OK"
fi

