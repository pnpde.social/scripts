#!/bin/bash

# This script checks loads a list of trash mail domains and adds them to the mastodon blocklist

# Variables
verbose=0
while getopts ":arvh" opt; do
case ${opt} in
 a)
   echo "Add Option is triggered."
   mode="add"
   ;;
 r)
   echo "Remove Option is triggered."
   mode="remove"
   ;;
 v)
   echo "Verbose Option is triggered"
   verbose=1
   ;;
 \?)
   echo "Invalid option: -${OPTARG}."
   ;;
 h)
   echo "valid options are"
   echo "-a add blocks"
   echo "-r remove blocks"
   echo "-v to display debug messages"
   exit 1
   ;;
esac
done
repoName="disposable-email-domains"
gitURL="https://github.com/$repoName/$repoName.git"

# Report active mode
if [ -z "$mode" ]; then
   echo "please select an option; use -h to see all options"
   exit
fi
echo "Selected mode is: $mode"

# Check user

activeUser=$(whoami)
if [ "$activeUser" = "mastodon" ]; then
    echo "active as user mastodon"
else 
    echo "Please run as user mastodon"
    exit
fi

# Check if download target is already there
if [ -d "/tmp/$repoName" ]; then
    rm -rf "/tmp/$repoName"
fi

cd /tmp
git clone "$gitURL"
cd "$repoName"

echo "Cloned Repository - Press Enter to continue..."
read

# Read blocklist file into a bash array
mapfile -t blocklist < disposable_email_blocklist.conf

# Validate domain entries
valid_domains=()
for domain in "${blocklist[@]}"; do
    if [[ $domain =~ ^[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$ ]]; then
        valid_domains+=("$domain")
    else
        echo "Invalid domain: $domain"
    fi
done

IFS=" " # Set space as the delimiter
joined_domains=$(printf "%s " "${valid_domains[@]}" | xargs) # Join and trim whitespace

if [ $verbose -gt 0 ]; then   echo "$joined_domains" | cat -A; fi
printf "%s\n" "${valid_domains[@]}" > domains.txt


CMD="/home/mastodon/live/bin/tootctl email-domain-blocks $mode \"$joined_domains\" || echo \"failed to $mode $domainList\""
printf "\n-----\nRunning $mode\n"

if [ $verbose -gt 0 ]; then printf $CMD; fi
RAILS_ENV=production xargs -a domains.txt /home/mastodon/live/bin/tootctl email-domain-blocks $mode

# Clean Up
echo "Ready to run cleanup - Press Enter to continue..."
read

cd ~
rm -rf "/tmp/$repoName"

printf "All done, exiting\n"