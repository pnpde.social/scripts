#!/bin/bash

set -eux

# create temp files
EMAIL_CONTENT=$(mktemp /tmp/email_content.XXXXXX)
COMPRESSED_CONTENT=$(mktemp /tmp/tripwire_email.gz.XXXXXX)

# get input from tripwire
cat - > "$EMAIL_CONTENT"

# compress all data from tripwire
gzip -c "$EMAIL_CONTENT" > "$COMPRESSED_CONTENT"

# gather necessary email data for skeleton mail
RECIPIENT=$(grep -i "^To: " "$EMAIL_CONTENT" | cut -d' ' -f2)
SUBJECT=$(grep -i "^Subject: " "$EMAIL_CONTENT" | cut -d' ' -f2-)
BODY="Tripwire report is attached as a compressed file."

# build skeleton mail and attach gzip and sent it
(
  echo "To: ${RECIPIENT}"
  echo "Subject: ${SUBJECT}"
  echo "MIME-Version: 1.0"
  echo "Content-Type: multipart/mixed; boundary=\"BOUNDARY\""
  echo
  echo "--BOUNDARY"
  echo "Content-Type: text/plain"
  echo
  echo "${BODY}"
  echo
  echo "--BOUNDARY"
  echo "Content-Type: application/gzip"
  echo "Content-Disposition: attachment; filename=\"tripwire_report.txt.gz\""
  echo
  cat "$COMPRESSED_CONTENT"
  echo
  echo "--BOUNDARY--"
) | /usr/sbin/sendmail -oi -t

# be a good script and remove temp data
rm "$EMAIL_CONTENT" "$COMPRESSED_CONTENT"
