#!/usr/bin/php
<?
    $opts = getopt("", [
        "delete:",
        "age:",
        "help",
    ]);
    $min_age = $opts['age'] ?? 1;
    $delete = $opts['delete'] ?? 0;
    $help = isset($opts['help']) ? 1 : 0;

    if($help) {
        print $argv[0]." [ --age 1 ]\n";
        print "   list bookstack users that have not confirmed their email within --age day\n";
        print "   and still have the registration role. --age is optional, default 1\n";
        print $argv[0]." [ --age 1 ] --delete <ID>\n";
        print "   delete bookstack user with ID\n";
        print $argv[0]." --help\n";
        print "   display this help text\n";
        exit;
    }

    require_once("/var/www/dbconfig.php");
    require_once("/var/www/lib/d3/class.DBConn.php");
    
    $dbconn = new DBConn();
    $db = $dbconn->connect('bookstack');
    
    $format = "% -2s% 4s %- 21s%- 36s%16s\n";
    
    // users that have not confirmed their email within 1 day and still have the registration role
    $sql = "SELECT u.id, u.name, u.email, unix_timestamp(u.created_at) 
        FROM `users` u
        join role_user ru on ru.user_id = u.id 
        where u.email_confirmed = 0 and u.created_at < now() - interval $min_age day
        and ru.role_id = (select value from settings where setting_key='registration-role')";
    
    if($delete && $delete != 'ALL') {
        $sql .= " and id='$delete'";
    }
    
    $suspicious_ids = [];
    $res = $db->query($sql);
    if($res->num_rows) {
        printf($format, "S", "ID", "NAME", "EMAIL", "CREATED_AT");
        $number=0;
        $upper=0;
        $lower=0;
        $other=0;
        while(list($id, $name, $email, $created_at) = $res->fetch_row()) {
            foreach(preg_split('//', $name) as $c) {
                if(is_numeric($c)) {
                    $number++;
                } else if(ctype_upper($c)) {
                    $upper++;
                } else if(ctype_lower($c)) {
                    $lower++;
                } else {
                    $other++;
                }
            }
            
            // mark suspicious names
            $suspicious = '';
            if($upper > 3 && $lower > 3) {
                $suspicious = '!';
                $suspicious_ids[] = $id;
            }
            if($other > $lower) {
                $suspicious = '!';
                $suspicious_ids[] = $id;
            }
            
            $last_name = $name;
            $last_id = $id;
            printf($format, $suspicious, $id, $name, $email, date("Y-m-d H:i", $created_at));
        }
    
        if($delete) {
            print "\n";
            
            if($delete=='ALL') {
                $input = readline("sure to delete ALL ".count($suspicious_ids)." accounts marked as suspicious (\"!\" in first column)? [y/N] ");
                if(preg_match('/^y|yes$/i', $input)) {
                    if($db->query("delete from users where id in (".join(",", $suspicious_ids).")")) {
                        print "ok, ".count($suspicious_ids)." deleted\n";
                    } else {
                        print "error: ".$db->error()."\n";
                    }
                } else {
                    print "ok, canceled\n";
                }
            } else if(isset($last_id) && $last_id == $delete) {
                $input = readline("delete user $last_name? [y/N] ");
            
                if(preg_match('/^y|yes$/i', $input)) {
                    if($db->query("delete from users where id = '$last_id'")) {
                        print "ok, 1 deleted\n";
                    } else {
                        print "error: ".$db->error()."\n";
                    }
                } else {
                    print "ok, canceled\n";
                }
            }
        }
    } else {
        print "none found\n";
    }
?>